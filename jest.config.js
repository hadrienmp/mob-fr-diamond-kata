
module.exports = {

    // The glob patterns Jest uses to detect test files
    testMatch: [
        //   "**/__tests__/**/*.[jt]s?(x)",
        "**/spec/**/*Spec.bs.js"
    ],
};
